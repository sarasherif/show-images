import Image from 'antd/lib/image';

function CustomImage({ imagUrl }) {
  
  return (
    <Image  width={300} height={300} src={imagUrl} />
  );
}
export default CustomImage;