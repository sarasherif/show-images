import Button from "antd/lib/button";
import "./CustomButton.scss";

function CustomButton({ onClick, isSelected, children, ...props }) {
  return (
    <Button
      type="primary"
      className={isSelected ? "button-clicked" : ""}
      onClick={onClick}
      {...props}
    >
      {children}
    </Button>
  );
}
export default CustomButton;
