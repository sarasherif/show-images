import React, { useState } from "react";
import Space from "antd/lib/space";
import CustomImage from "./components/CustomImage/CustomImage";
import CustomButton from "./components/CustomButton/CustomButton";

const btnsToImages = [
  {
    btnText: "Image one",
    imgSrc: "./imageOne.jpeg",
  },
  {
    btnText: "Image two",
    imgSrc: "./imageTwo.jpg",
  },
  {
    btnText: "Image three",
    imgSrc: "./imageThree.jpeg",
  },
];

function App() {
  const [selected, setSelected] = useState("./default.png");

  return (
    <Space>
      <CustomImage imagUrl={selected} />

      {btnsToImages.map((btn, i) => (
        <CustomButton
          key={i}
          onClick={() => setSelected(btn.imgSrc)}
          isSelected={selected === btn.imgSrc}
        >
          {btn.btnText}
        </CustomButton>
      ))}
    </Space>
  );
}

export default App;

